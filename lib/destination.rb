require_relative 'introductory'
require 'sax-machine'
require 'forwardable'

class Destination
  include SAXMachine
  extend Forwardable

  attribute :atlas_id
  attribute :title,        :as => :name
  element   :introductory, :as => :introductory, :class => Introductory
  element   :history,      :as => :brief_history do |brief_history|
    brief_history.strip
  end

  def_delegator :@introductory, :overview, :overview
  def_delegator :@introductory, :travel_alert, :travel_alert
  
  def normalized_name
    name.downcase.gsub(/[^\w\.]/, '_')
  end
 end
