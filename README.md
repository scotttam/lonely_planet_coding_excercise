# Lonely Planet Coding Exercise
[![Circle CI](https://circleci.com/gh/scotttam/lonely_planet.svg?style=svg&circle-token=f80feb7a974f700a91b3d123cdd9cde79c3211fa)](https://circleci.com/gh/scotttam/lonely_planet)

## Installation and Running the example
1. Ensure ruby 2.2.0 is installed in RVM. rbenv and other versions of ruby are theoretically possible but not tested in this exercise.
2. Clone this repo locally.
3. cd into the base of this repo.
4. Install bundler if it hasn't been already.

 ```bash
 $ gem install bundler
 ```
5. Install the necessary gems.

 ```bash
 $ bundle install
 ```
6. At this point, you should be able to run the code successfully. Here's the details:

 ```bash
 $ ruby lp_test.rb
 Usage: lp_test.rb [options]

 Required options:
     -l, --input_dir INPUT            The dir where the destination.xml and taxonomy.xml files are located.
     -o, --output_dir OUTPUT          The output directory. The directory must exist, will not be automatically created.

 Other options:
     -h, --help                       Show this message
         --version                    Show version
 ```

## Running the test suite
The test suite is simply run by:

 ```bash
 $ rake test
 ```

## Notes
 * Used SAX rather than DOM parser for performance and memory reasons
 * XPATH is slow, use SAX parsing and child iterating instead
 * I prefer RSpec but for the sake of less gems, I used MiniTest.
 * I used the shoulda gem as MiniTest out of the box is painful to read, IMO.
 * Using CircleCI for Continuous Integration
