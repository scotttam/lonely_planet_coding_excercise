require 'benchmark'

class Diagnostics
  # Benchmark columns are:
  #  This process' CPU time - this is the number to worry about
  #  System CPU time (Kernel OS time)
  #  The sum of the user and system CPU times
  #  Elapsed real time
  def self.with_timing(str, logger)
    logger.warn(str)
    results = Benchmark.measure { yield }
    logger.warn("Done ( #{results.to_s} )")
  end

  def self.stats(files_written, logger)
    pid, size = `ps ax -o pid,rss | grep -E "^[[:space:]]*#{$$}"`.strip.split.map(&:to_i)
    logger.warn("Pid: #{pid} Wrote #{files_written} files. Memory usage: #{size/1000.0} MB")
  end
end