require 'sax-machine'

class Node
  include SAXMachine

  attribute :atlas_node_id, :as => :atlas_id
  element   :node_name,     :as => :name
  elements  :node,          :as => :nodes, :class => Node

  attr_accessor :parent_node
  alias :child_nodes :nodes

  def initialize
    @parent_node = nil
  end
end
