require 'optparse'
require 'optparse/time'
require 'ostruct'

class Options
  VERSION = '0.1'

  def self.print_help_and_exit(opt_parser, header=nil)
    if header
      puts header
      puts ''
    end

    puts opt_parser.banner
    puts opt_parser.summarize
    exit
  end

  def self.parse(args)
    options = OpenStruct.new(:input_dir => '', :output_dir => '')

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: lp_test.rb [options]'
      opts.separator ''
      opts.separator 'Required options:'

      opts.on('-l', '--input_dir INPUT',
              'The dir where the destination.xml and taxonomy.xml files are located.') do |input|
        options.input_dir << input
        if !File.exist?("#{options.input_dir}/destinations.xml") || !File.exist?("#{options.input_dir}/taxonomy.xml")
          print_help_and_exit(opts, 'Could not find either destinations or taxonomy.xml files')
        end
      end

      opts.on('-out', '--output_dir OUTPUT',
              'The output directory. The directory must exist, will not be automatically created.') do |output|
        options.output_dir << output
        if !File.directory?(options.output_dir)
          print_help_and_exit(opts, 'Output directory does not exist. Please create it.')
        end
        
        if !File.writable?(options.output_dir)
          print_help_and_exit(opts, "It seems that the #{options.output_dir} directory is not writable. Please make writable for the user #{ENV['USER']}.")
        end
      end

      opts.separator ''
      opts.separator 'Other options:'

      opts.on_tail('-h', '--help', 'Show this message') do
        print_help_and_exit(opts)
      end

      opts.on_tail('--version', 'Show version') do
        puts VERSION
        exit
      end
    end

    opt_parser.parse!(args)

    if options.input_dir.empty? || options.output_dir.empty?
      print_help_and_exit(opt_parser, 'Missing required params.')
    end

    options
  end
end
