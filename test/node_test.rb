require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/node'
require_relative '../lib/taxonomy_parser'

class NodeTest < Minitest::Test
  context 'a node' do
    setup do
      world_node = OpenStruct.new(:name => 'World', :child_nodes => [], :parent_node => nil)
      world_dest = OpenStruct.new(:name => 'World', :normalized_name => 'world', :atlas_id => '0',
                                  :overview => 'World', :travel_alert => '', :brief_history => '')

      @taxonomy_xml = File.join(File.dirname(__FILE__), 'xml_files', 'taxonomy.xml')
      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
      @tax_parser = TaxonomyParser.new
      @destination_parser = DestinationParser.new
      @root_taxonomy_nodes = @tax_parser.parse(File.read(@taxonomy_xml)).nodes
      destinations = @destination_parser.parse(File.read(@destination_xml)).destinations
      africa_destination = destinations.first
      @africa_node = @tax_parser.find_destination_node(africa_destination, world_node)
    end

    context 'attributes and elements' do
      should 'contain atlas_id, name and any child nodes' do
        assert_equal '355064', @africa_node.atlas_id
        assert_equal 'Africa', @africa_node.name
        assert_equal Node, @africa_node.class
      end
    end

    context '#child_nodes' do
      should 'return all the immediate child nodes for the given node' do
        africa_child_nodes = ['South Africa', 'Sudan', 'Swaziland']
        assert_equal africa_child_nodes, @africa_node.child_nodes.map(&:name)
      end

      should 'return empty array if there are not any children' do
        @table_mountian = @africa_node.nodes.first.nodes.first.nodes.first
        assert_equal [], @table_mountian.child_nodes.map(&:name)
      end
    end
  end
end
