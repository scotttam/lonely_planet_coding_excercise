require_relative 'destination'
require 'sax-machine'

class DestinationParser
  include SAXMachine

  elements :destination, :as => :destinations, :class => Destination
end
