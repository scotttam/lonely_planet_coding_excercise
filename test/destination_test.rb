require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/destination'

class DestinationTest < Minitest::Test
  context 'a destination' do

    setup do
      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
      @destination_parser = DestinationParser.new
      @destinations = @destination_parser.parse(File.read(@destination_xml)).destinations
      @destination = @destinations.first
    end

    should 'contain atlas_id, name, overview and brief_history' do
      assert_equal '355064', @destination.atlas_id
      assert_equal 'Africa', @destination.name
      assert_equal 'How do you capture the essence of Africa o', @destination.overview[0, 42]
      assert_equal 'You’ve probably heard the claim that Afric', @destination.brief_history[0, 42]
      assert_equal Destination, @destination.class
    end

    context 'methods that delgate to Introductory' do
      context 'overview' do
        should 'contain the overview' do
          assert_equal 'How do you capture the essence of Africa o', @destination.overview[0, 42]
        end
      end

      context '#travel_alert' do
        should 'return the travel alert if there is one' do
          @travel_alert_destinations = @destinations.select { |destination| !destination.travel_alert.nil? }
          @travel_alert_destinations.each do |tad|
            assert tad.travel_alert, 'Travel alert should not be nil'
          end
        end

        should 'be nil if there isnt any travel alert' do
          assert !@destination.travel_alert
        end
      end
    end
    
    context '#normalized_name' do
      setup do
        @tmnp = @destinations.find { |destination| destination.name == 'Table Mountain National Park' }
      end

      should 'downcase and underscore the name' do
        assert_equal 'table_mountain_national_park', @tmnp.normalized_name
      end

      should 'replace any bad chars with underscore' do
        @tmnp.name = 'M?ya!nm*a"r/B<u>r\m:a'
        assert_equal 'm_ya_nm_a_r_b_u_r_m_a', @tmnp.normalized_name
      end
    end
  end
end
