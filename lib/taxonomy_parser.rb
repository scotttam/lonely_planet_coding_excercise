require_relative 'node'
require 'sax-machine'

class TaxonomyParser
  include SAXMachine

  elements :node, :as => :nodes, :class => Node

  def find_destination_node(destination, world)
    @dest_node = nil
    @parent_node = nil
    traverse_down(nil, nodes, destination.atlas_id)

    if @parent_node.nil?
      @dest_node.parent_node  = world
      world.child_nodes << @dest_node
    else
      @dest_node.parent_node = @parent_node
    end
    @dest_node
  end

  private

  def traverse_down(parent_node, sub_nodes, atlas_id)
    return if sub_nodes.empty? || !@dest_node.nil?
    sub_nodes.each do |node|
      if node.atlas_id.to_s == atlas_id.to_s
        @dest_node = node
        @parent_node = parent_node
        break
      else
        traverse_down(node, node.nodes, atlas_id)
      end
    end
  end
end
