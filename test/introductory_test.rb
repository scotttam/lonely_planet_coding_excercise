require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/introductory'

class IntroductoryTest < Minitest::Test
  context 'a introductory' do

    setup do
      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
      @destination_parser = DestinationParser.new
      @destinations = @destination_parser.parse(File.read(@destination_xml)).destinations
      @introductory = @destinations.first.introductory
      @travel_alert_destinations = @destinations.select { |destination| !destination.travel_alert.nil? }
    end

    should 'contain the overview' do
      assert_equal 'How do you capture the essence of Africa o', @introductory.overview[0, 42]
    end

    context "#overview" do
      should 'not contain the travel alert, it should be parsed out' do
        assert !@travel_alert_destinations.first.overview.include?('Travel Alert:')
      end
    end

    context '#travel_alert' do
      should 'parse out the travel alert from the content if it is there' do
        assert_equal 3, @travel_alert_destinations.size, 'There should be exactly three destinations with travel alerts'
        assert_equal 'Travel Alert: Crime is a problem throughout South Africa; see the Health & Safety section for details.', @travel_alert_destinations[0].travel_alert
        assert_equal 'Travel Alert: Crime is a serious problem in Johannesburg; see the Dangers & Annoyances section for details.', @travel_alert_destinations[1].travel_alert
        assert_equal 'Travel Alert: The security situation in Sudan is highly unstable and several areas, particularly in and around Darfur, are no-go zones.', @travel_alert_destinations[2].travel_alert
      end

      should 'be nil if there isnt any travel alert' do
        assert !@destinations.first.travel_alert
      end
    end
  end
end
