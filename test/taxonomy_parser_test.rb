require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/taxonomy_parser'
require_relative '../lib/destination_parser'

class TaxonomyTest < Minitest::Test
  context 'a taxonomy parser' do
    setup do
      @world_node = OpenStruct.new(:name => 'World', :child_nodes => [], :parent_node => nil)
      world_dest = OpenStruct.new(:name => 'World', :normalized_name => 'world', :atlas_id => '0',
                                  :overview => 'World', :travel_alert => '', :brief_history => '')

      @taxonomy_xml = File.join(File.dirname(__FILE__), 'xml_files', 'taxonomy.xml')
      @tax_parser = TaxonomyParser.new.parse(File.read(@taxonomy_xml))
      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
      @destination_parser = DestinationParser.new
      destinations = @destination_parser.parse(File.read(@destination_xml)).destinations
      @africa_destination = destinations.first
      @table_mountian_destination = destinations.find { |d| d.name == 'Table Mountain National Park' }

    end

    context '#parse' do
      should 'return an array of taxonomy nodes' do
        assert_equal Array, @tax_parser.nodes.class
        assert_equal 1, @tax_parser.nodes.size
        assert_equal Node, @tax_parser.nodes.first.class
      end
    end

    context '#find_destination_node' do
      should 'find the destination node and set all the parent nodes' do
        node = @tax_parser.find_destination_node(@table_mountian_destination, @world_node)
        assert_equal [], node.nodes.map(&:name)
        assert_equal 'Cape Town', node.parent_node.name
      end

      should 'set the parent node to World if there are not any parents' do
        node = @tax_parser.find_destination_node(@africa_destination, @world_node)
        assert_equal ["South Africa", "Sudan", "Swaziland"], node.nodes.map(&:name)
        assert_equal 'World', node.parent_node.name
        assert_equal ['Africa'], @world_node.child_nodes.map(&:name)
      end
    end
  end
end