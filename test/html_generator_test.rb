require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/html_generator'
require_relative '../lib/destination_parser'
require_relative '../lib/taxonomy_parser'

class HtmlGeneratorTest < Minitest::Test
  
  context 'a generator' do
    setup do
      @world_node = OpenStruct.new(:name => 'World', :child_nodes => [], :parent_node => nil)
      world_dest = OpenStruct.new(:name => 'World', :normalized_name => 'world', :atlas_id => '0',
                                  :overview => 'World', :travel_alert => '', :brief_history => '')

      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
      @taxonomy_xml = File.join(File.dirname(__FILE__), 'xml_files', 'taxonomy.xml')
      @destinations = DestinationParser.new.parse(File.read(@destination_xml)).destinations
      @tax_parser = TaxonomyParser.new.parse(File.read(@taxonomy_xml))
      @africa_destination = @destinations.first
    end

    context '#render' do
      setup do
        africa_node = @tax_parser.find_destination_node(@africa_destination, @world_node)
        erb = HtmlGenerator.new(@africa_destination, africa_node)
        @rendered_html = erb.render
      end

      should 'render HTML' do
        html = Nokogiri::HTML.parse(@rendered_html)
        parsed_overview = html.xpath("//div[@id='main']/div/div[@class='content']/div[@class='inner']/div[@class='overview']").inner_html.split('<h4>Overview</h4>').last.strip
        parsed_super_nav = html.xpath("//div[@id='navcontainer']/ul/li/a").first.inner_html
        parsed_nav_current_destination = html.xpath("//li[@class='destination']").first.inner_html.strip.split(' ').first
        parsed_sub_nav = html.xpath("//li[@class='destination']/ul/li/a").map(&:inner_html)
        
        assert_equal 'World', parsed_super_nav
        assert_equal 'Africa', parsed_nav_current_destination
        assert_equal ['South Africa', 'Sudan', 'Swaziland'], parsed_sub_nav
        assert_equal 'How do you capture the essence of Africa o', parsed_overview[0, 42]
      end
    end
  end
end
