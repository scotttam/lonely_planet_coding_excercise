require 'minitest/autorun'
require 'shoulda/context'

require_relative '../lib/destination_parser'
require_relative '../lib/destination'

class DestinationTest < Minitest::Test
  context 'a destination parser' do
    setup do
      @destination_xml = File.join(File.dirname(__FILE__), 'xml_files', 'destinations.xml')
    end

    context '#parse' do
      should 'return an array of destinations' do
        @destination_parser = DestinationParser.new
        destinations = @destination_parser.parse(File.read(@destination_xml))
        assert_equal Array, destinations.destinations.class
        assert_equal 24, destinations.destinations.size
        assert_equal Destination, destinations.destinations.first.class
      end
    end
  end
end
