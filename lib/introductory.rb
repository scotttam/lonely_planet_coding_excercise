require 'sax-machine'
require 'pp'

class Introductory
  include SAXMachine

  attr_reader :travel_alert

  element :overview, :as => :overview do |overview|
    parse_travel_alert(overview.strip)
  end

  def initialize
    @travel_alert = nil
  end

  private

  def parse_travel_alert(overview)
    return overview unless overview.start_with?('Travel Alert:')
    overview.split('.', 2).tap do |parts|
      @travel_alert = parts.first + '.'
    end.last
  end
end
