require 'erb'

class HtmlGenerator
  BASE_TEMPLATE_PATH = 'template/base_template.html.erb'
  extend ERB::DefMethod
  def_erb_method('render()', BASE_TEMPLATE_PATH)

  def initialize(destination, dest_taxonomy_node)
    @destination_name = destination.name
    @atlas_id = destination.atlas_id
    @overview = destination.overview
    @travel_alert = destination.travel_alert
    @brief_history = destination.brief_history
    @sub_navi = dest_taxonomy_node.child_nodes.map { |child| child.name.gsub("'", '') }
    @super_navi = dest_taxonomy_node.parent_node.nil? ? [] : [dest_taxonomy_node.parent_node.name.gsub("'", '')]
  end
end
