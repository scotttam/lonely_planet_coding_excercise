require 'fileutils'
require 'logger'
Dir[File.dirname(__FILE__) + '/lib/*.rb'].each { |file| require_relative file }

options = Options.parse(ARGV)
logger = Logger.new(STDOUT)
world_node = OpenStruct.new(:name => 'World', :child_nodes => [], :parent_node => nil)
world_dest = OpenStruct.new(:name => 'World', :normalized_name => 'world', :atlas_id => '0',
                            :overview => 'World', :travel_alert => '', :brief_history => '')

Diagnostics.with_timing('Beginning run... ', logger) do
  tax_parser = TaxonomyParser.new.parse(File.read("#{options.input_dir}/taxonomy.xml"))
  dest_parser = DestinationParser.new.parse(File.read("#{options.input_dir}/destinations.xml"))
  destinations = dest_parser.destinations

  logger.warn "Copying CSS files to #{options.output_dir}... "
  %w(all enhancements).each { |css_file| FileUtils.cp("template/#{css_file}.css", "#{options.output_dir}/#{css_file}.css") }
  logger.warn 'Done'

  logger.warn "Begin writing html files to #{options.output_dir}"
  files_written = 0

  destinations.each do |destination|
    dest_node = tax_parser.find_destination_node(destination, world_node)
    erb = HtmlGenerator.new(destination, dest_node)
    File.open("#{options.output_dir}/#{destination.normalized_name}.html", 'w') do |f|
      f.write(erb.render)
    end
    files_written += 1
    Diagnostics.stats(files_written, logger) if files_written % 1000 == 0
  end

  erb = HtmlGenerator.new(world_dest, world_node)
  File.open("#{options.output_dir}/#{world_dest.normalized_name}.html", 'w') do |f|
    f.write(erb.render)
  end

  logger.warn "Done writing html files. Wrote #{files_written} files."
end